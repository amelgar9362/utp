package com.prueba.utp.service.impl;

import com.prueba.utp.dao.NotaDao;
import com.prueba.utp.entity.Curso;
import com.prueba.utp.entity.Nota;
import com.prueba.utp.entity.request.NotaRequest;
import com.prueba.utp.entity.response.NotaResponse;
import com.prueba.utp.service.NotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NotaServiceImpl implements NotaService {

    @Autowired
    private NotaDao notaDao;
    @Override
    public NotaResponse save(NotaRequest request) {
        NotaResponse response = new NotaResponse();
        Nota  notaEntity =  new Nota();
        notaEntity.setNota1(request.getNota1());
        notaEntity.setNota2(request.getNota2());
        notaEntity.setNota3(request.getNota3());
        Integer promedio =
                (Integer.parseInt(notaEntity.getNota1()) + Integer.parseInt(notaEntity.getNota2()) + Integer.parseInt(notaEntity.getNota3()))/3;
        notaEntity.setPromedio(promedio.toString());
        Curso cursoId = new Curso();
        cursoId.setIdCurso(request.getIdCurso());
        notaEntity.setCurso(cursoId);
        try {
            Nota nota = notaDao.save(notaEntity);
            if(Objects.nonNull(nota)){
                response.getNota().add(nota);
                response.setCodigo("0");
                response.setMensaje("Exitoso");
            }else{
                response.getNota().add(nota);
                response.setCodigo("1");
                response.setMensaje("No Exitoso");
            }
        }catch (Exception ex){
            response.setCodigo(ex.getCause().getMessage());
            response.setMensaje("Error al Guardar" + ex.getMessage());

        }
        return response;
    }

    @Override
    public NotaResponse update(String id,NotaRequest request) {
        NotaResponse response = new NotaResponse();
        Nota notaEntity =  new Nota();
        notaEntity.setNota1(request.getNota1());
        notaEntity.setNota2(request.getNota2());
        notaEntity.setNota3(request.getNota3());
        Integer promedio =
                (Integer.parseInt(notaEntity.getNota1()) + Integer.parseInt(notaEntity.getNota2()) + Integer.parseInt(notaEntity.getNota3()))/3;
        notaEntity.setPromedio(promedio.toString());
        try {
            Optional<Nota> nota = notaDao.findById(Integer.parseInt(id));
            if(nota.isPresent()){
                Nota notaUpdate = nota.get();
                notaUpdate = notaDao.save(notaEntity);
                response.getNota().add(notaUpdate);
                response.setCodigo("0");
                response.setMensaje("Exitoso");
            }else{
                response.setNota(null);
                response.setCodigo("1");
                response.setMensaje("No Exitoso");
            }
        }catch (Exception ex){
            response.setCodigo(ex.getCause().getMessage());
            response.setMensaje("Error al Guardar" + ex.getMessage());

        }
        return response;
    }

    @Override
    public List<NotaResponse> getAll() {
        List<Nota> nota = notaDao.findAll();
        return  nota.stream().map(p -> {
            NotaResponse response = new NotaResponse();
            response.getNota().add(p);
            return response;
        }).collect(Collectors.toList());
    }
}
