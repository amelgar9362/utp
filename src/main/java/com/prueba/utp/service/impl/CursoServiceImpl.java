package com.prueba.utp.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.utp.dao.CursoDao;
import com.prueba.utp.entity.Curso;
import com.prueba.utp.entity.request.CursoRequest;
import com.prueba.utp.entity.response.CursoResponse;
import com.prueba.utp.service.CursoService;

@Service
public class CursoServiceImpl implements CursoService {

    @Autowired
    private CursoDao cursoDao;
    @Override
    public CursoResponse save(CursoRequest request) {
        CursoResponse response = new CursoResponse();
        Curso cursoEntity =  new Curso();
        cursoEntity.setNombreCurso(request.getNombreCurso());
        try {
            Curso curso = cursoDao.save(cursoEntity);
            if(Objects.nonNull(curso)){
                response.getCurso().add(curso);
                response.setCodigo("0");
                response.setMensaje("Exitoso");
            }else{
                response.setCurso(null);
                response.setCodigo("1");
                response.setMensaje("No Exitoso");
            }
        }catch (Exception ex){
            response.setCodigo(ex.getCause().getMessage());
            response.setMensaje("Error al Guardar" + ex.getMessage());

        }
        return response;
    }

    @Override
    public CursoResponse update(String id,CursoRequest request) {
        CursoResponse response = new CursoResponse();
        Curso cursoEntity =  new Curso();
        cursoEntity.setNombreCurso(request.getNombreCurso());
        try {
            Optional<Curso> curso = cursoDao.findById(Integer.parseInt(id));
            if(curso.isPresent()){
                Curso cursoUpdate = curso.get();
                cursoUpdate = cursoDao.save(cursoEntity);
                response.getCurso().add(cursoUpdate);
                response.setCodigo("0");
                response.setMensaje("Exitoso");
            }else{
                response.setCurso(null);
                response.setCodigo("1");
                response.setMensaje("No Exitoso");
            }
        }catch (Exception ex){
            response.setCodigo(ex.getCause().getMessage());
            response.setMensaje("Error al Guardar" + ex.getMessage());

        }
        return response;
    }

    @Override
    public List<CursoResponse> getAll() {
        List<Curso> curso = cursoDao.findAll();
        return  curso.stream().map(p -> {
            CursoResponse response = new CursoResponse();
            response.getCurso().add(p);
            return response;
        }).collect(Collectors.toList());
    }
}
