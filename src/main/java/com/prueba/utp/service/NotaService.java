package com.prueba.utp.service;

import com.prueba.utp.entity.request.NotaRequest;
import com.prueba.utp.entity.response.NotaResponse;

import java.util.List;

public interface NotaService {
    NotaResponse save(NotaRequest request);

    NotaResponse update(String id,NotaRequest request);

    List<NotaResponse> getAll();
}
