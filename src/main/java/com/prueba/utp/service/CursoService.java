package com.prueba.utp.service;

import com.prueba.utp.entity.request.CursoRequest;
import com.prueba.utp.entity.response.CursoResponse;

import java.util.List;

public interface CursoService {
    CursoResponse save(CursoRequest request) ;

    CursoResponse update(String id,CursoRequest request);

    List<CursoResponse> getAll();


}
