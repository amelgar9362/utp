package com.prueba.utp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.utp.entity.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>  {

	//from usuario where username = ?
	//@Query("FROM Usuario us where us.username = ?")
	//Derived Query
	Usuario findOneByUsername(String username);	
}
