package com.prueba.utp.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.utp.entity.Curso;

@Repository
public interface CursoDao extends JpaRepository<Curso , Integer> {
}
