package com.prueba.utp.dao;

import com.prueba.utp.entity.Nota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotaDao extends JpaRepository<Nota, Integer > {
}
