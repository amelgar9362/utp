package com.prueba.utp.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.utp.entity.request.NotaRequest;
import com.prueba.utp.entity.response.NotaResponse;
import com.prueba.utp.service.NotaService;

@RestController
@RequestMapping("/notas")
public class NotaController {
    @Autowired
    private NotaService notaService;

    @PostMapping
    ResponseEntity<NotaResponse> save(@RequestBody @Valid NotaRequest request){
        NotaResponse response = notaService.save(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    ResponseEntity<NotaResponse> update(@PathVariable String id, @RequestBody @Valid NotaRequest request){

        NotaResponse response = notaService.update(id,request);


        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping()
    ResponseEntity<List<NotaResponse>> getAll(){

        List<NotaResponse> response = notaService.getAll();


        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
}
