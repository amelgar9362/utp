package com.prueba.utp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.utp.entity.request.CursoRequest;
import com.prueba.utp.entity.response.CursoResponse;
import com.prueba.utp.service.CursoService;

@RestController
@RequestMapping("/cursos")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping
    ResponseEntity<CursoResponse> save(@RequestBody @Valid CursoRequest request){
        CursoResponse response = cursoService.save(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    ResponseEntity<CursoResponse> update(@PathVariable String id, @RequestBody @Valid CursoRequest request){

        CursoResponse response = cursoService.update(id,request);


        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping()
    ResponseEntity<List<CursoResponse>> getAll(){

        List<CursoResponse> response = cursoService.getAll();


        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

}
