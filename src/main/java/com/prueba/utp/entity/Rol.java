package com.prueba.utp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Rol {

	@Id
	private Integer idRol;

	@Column(name = "nombre", nullable = false, length = 15)
	private String nombre;

	@Column(name = "descripcion", nullable = true, length = 150)
	private String descricpion;

	
}
