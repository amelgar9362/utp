package com.prueba.utp.entity.request;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CursoRequest {
    @NotNull(message = "No puede ser nulo nombreCurso")
    @NotBlank(message = "No puede ser blanco nombreCurso")
    private String nombreCurso;

}
