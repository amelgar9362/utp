package com.prueba.utp.entity.request;


import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class NotaRequest {
    @NotNull(message = "nota1 no puede ser nulo")
    private String nota1;
    @NotNull(message = "nota2 no puede ser nulo")
    private String nota2;
    @NotNull(message = "nota3 no puede ser nulo")
    private String nota3;

    @NotNull(message = "idCurso no puede ser nulo")
    private Integer idCurso;

}
