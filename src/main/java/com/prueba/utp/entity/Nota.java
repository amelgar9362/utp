package com.prueba.utp.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class Nota {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idNota;

    private String nota1;

    private String nota2;

    private String nota3;

    private String promedio;

    @ManyToOne
    @JoinColumn(name = "idCurso", insertable = false, updatable = false)
    private Curso curso;
}
