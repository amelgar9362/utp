package com.prueba.utp.entity.response;

import com.prueba.utp.entity.Nota;
import lombok.Data;

import java.util.List;

@Data
public class NotaResponse extends MensajeResponse{
    private List<Nota> nota;
}
