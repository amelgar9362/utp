package com.prueba.utp.entity.response;

import com.prueba.utp.entity.Curso;
import lombok.Data;

import java.util.List;

@Data
public class CursoResponse extends MensajeResponse{
    private List<Curso> curso;
}
