package com.prueba.utp.entity.response;

import lombok.Data;

@Data
public class MensajeResponse {
    private String codigo;
    private String mensaje;
}
