package com.prueba.utp.entity;




import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;


@Entity
@Data
public class Curso {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idCurso;

    private String nombreCurso;
    
//	@OneToMany(mappedBy = "curso")
//	private List<Nota> nota;


}
